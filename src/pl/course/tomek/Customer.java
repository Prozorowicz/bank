package pl.course.tomek;

import java.util.ArrayList;

public class Customer {
    private String customerName;
    private ArrayList<Double> customersTransactions;

    public Customer(String customerName, double initialTransaction) {
        this.customerName = customerName;
        this.customersTransactions = new ArrayList<Double>();
        this.customersTransactions.add(initialTransaction);
    }

    public String getCustomerName() {
        return customerName;
    }

    public void addTransaction(double transaction) {
        this.customersTransactions.add(transaction);
    }

    public void listTransactions(){
        for (int i = 0; i < customersTransactions.size(); i++) {
            System.out.println("\t"+customersTransactions.get(i));
        }
    }
}
