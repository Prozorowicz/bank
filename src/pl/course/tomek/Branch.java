package pl.course.tomek;

import java.util.ArrayList;

public class Branch {
    private String branchName;
    private ArrayList<Customer> customerArrayList;

    public Branch(String branchName) {
        this.branchName = branchName;
        this.customerArrayList = new ArrayList<Customer>();
    }

    public String getBranchName() {
        return branchName;
    }



    public void addCustomer(String customerName, double initialTransaction) {
        if (inCustomerDatabase(customerName)==null) {
            customerArrayList.add(new Customer(customerName, initialTransaction));
            System.out.println("New customer "+customerName+" added.");
        }else {
            System.out.println("Customer with specified name already exists.");
        }
    }
    public void addTransactionToCustomer(String customerName, double transaction) {
        Customer customer = inCustomerDatabase(customerName);
        if (customer!=null) {
            customer.addTransaction(transaction);
            System.out.println("New transaction added for customer "+customerName+".");
        }else {
            System.out.println("Customer with specified name not found.");
        }
    }
    public void customersList(boolean showTransactions){
        for (int i = 0; i < customerArrayList.size(); i++) {
            System.out.println(customerArrayList.get(i).getCustomerName());
            if (showTransactions){
                customerArrayList.get(i).listTransactions();
            }
        }
    }

    private Customer inCustomerDatabase(String customerName) {
        for (int i = 0; i < customerArrayList.size(); i++) {
            Customer customer = customerArrayList.get(i);
            if (customer.getCustomerName().equals(customerName)) {
                return customer;
            }
        }
        return null;
    }

}
