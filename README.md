# Bank:

* Implementation of a simple banking app with following capabilities: adding branches, adding customers to branches, adding transactions to customers, listing customers.

# What I Learned:

* Working with ArrayLists and Autoboxing.