package pl.course.tomek;

import java.util.ArrayList;

public class Bank {
    private String bankName;
    private ArrayList<Branch> branchArrayList ;

    public Bank(String bankName) {
        this.bankName = bankName;
        this.branchArrayList = new ArrayList<Branch>();
    }

    public String getBankName() {
        return bankName;
    }

    public void addBranch(String branchName) {
        if (inBranchDatabase(branchName) == null) {
            branchArrayList.add(new Branch(branchName));
            System.out.println("New branch " + branchName + " added.");
        } else {
            System.out.println("Branch with specified name already exists.");
        }
    }

    public void addCustomerToBranch(String branchName, String customerName, double initialTransaction) {
        Branch branch = inBranchDatabase(branchName);
        if (branch != null) {
            System.out.println("In branch " + branchName + ":");
            branch.addCustomer(customerName, initialTransaction);
        } else {
            System.out.println("Branch with specified name not found.");
        }
    }

    public void addTransactionToCustomerInBranch(String branchName, String customerName, double transaction) {
        Branch branch = inBranchDatabase(branchName);
        if (branch != null) {
            System.out.println("In branch " + branchName + ":");
            branch.addTransactionToCustomer(customerName,transaction);
        } else {
            System.out.println("Branch with specified name not found.");
        }
    }
    public void customersListInBranch(String branchName,boolean showTransactions){
        Branch branch = inBranchDatabase(branchName);
        if (branch != null ){
            System.out.println("List of customers in branch " + branchName + ":");
           branch.customersList(showTransactions);
        } else {
            System.out.println("Branch with specified name not found.");
        }
    }

    private Branch inBranchDatabase(String branchName) {
        for (int i = 0; i < branchArrayList.size(); i++) {
            Branch branch = branchArrayList.get(i);
            if (branch.getBranchName().equals(branchName)) {
                return branch;
            }
        }
        return null;
    }
}
