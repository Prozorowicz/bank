package pl.course.tomek;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Bank bank = new Bank("Sknerus");
        Scanner scanner = new Scanner(System.in);
        boolean quit = false;
        int choice=0;
        printInstructions();
        while (!quit){
            System.out.println("\nWelcome to "+bank.getBankName()+" bank, press a number between 0 and 5(press 0 to show instructions):");
            choice=scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 0:
                    printInstructions();
                    break;
                case 1:
                    System.out.println("Enter new branch name:");
                    bank.addBranch(scanner.nextLine());
                    break;
                case 2:
                    System.out.println("You are adding a new customer, enter branch to which he/she should be assigned to:");
                    String newCustomerBranch=scanner.nextLine();
                    System.out.println("You are adding a new customer, enter his/hers name:");
                    String newCustomerName=scanner.nextLine();
                    System.out.println("You are adding a new customer, enter his/hers initial transaction:");
                    bank.addCustomerToBranch(newCustomerBranch,newCustomerName,scanner.nextDouble());
                    scanner.nextLine();
                    break;
                case 3:
                    System.out.println("You are adding a transaction to a customer, enter branch to which he/she is assigned to:");
                    String customerBranch=scanner.nextLine();
                    System.out.println("You are adding a transaction to a customer, enter his/hers name:");
                    String customerName=scanner.nextLine();
                    System.out.println("You are adding a transaction to a customer, enter his/hers transaction:");
                    bank.addTransactionToCustomerInBranch(customerBranch,customerName,scanner.nextDouble());
                    scanner.nextLine();
                    break;
                case 4:
                    System.out.println("Enter name of a branch you want to view customers from:");
                    String branch = scanner.nextLine();
                    System.out.println("Should the list include customers transactions?(Enter 'y' for yes or 'n' for no):");
                    boolean showTransactions=false;
                    if (scanner.nextLine().equals("y")){
                        showTransactions=true;
                    }
                    bank.customersListInBranch(branch,showTransactions);
                    break;
                case  5:
                    System.out.println("Exiting application.");
                    quit=true;
                    break;
            }
        }

    }

    public static void printInstructions() {
        System.out.println("\nEnter:");
        System.out.println("\t0 - to print instructions.");
        System.out.println("\t1 - to add a new branch.");
        System.out.println("\t2 - to add a new customer.");
        System.out.println("\t3 - to add a transaction.");
        System.out.println("\t4 - to list customers.");
        System.out.println("\t5 - to exit.");

    }
}
